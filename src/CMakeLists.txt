find_library(seccomp_lib NAMES seccomp)
add_library(sandbox sandbox.cc)
add_library(myopen SHARED myopen.cc sandbox)
add_executable(proba proba.cc)
add_executable(daemon daemon.cc)

target_link_libraries(daemon sandbox)
target_link_libraries(myopen ${seccomp_lib})
