#include "daemon.hpp"
#include "debug.hpp"
#include "sandbox.hpp"
#include <fcntl.h>
#include <poll.h>
#include <regex>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

static volatile sig_atomic_t OK = 1;

static void
sighandler(int signal, siginfo_t* info, void* context)
{
  if (signal == SIGUSR1) {
    OK = 0;
  }
}

enum
{
  MASTER_SOCKET = 1,
  MAX_SOCK = MASTER_SOCKET + 1,
};

// Allow /proc /sys /dev /etc /usr /lib
const std::regex FilterDaemon::_allow("^\\/(proc|sys|dev|etc|usr|lib)\\/.*?");
// Deny relative paths that would jailbreak
const std::regex FilterDaemon::_deny(".*\\.\\..*");

FilterDaemon::FilterDaemon(std::string& path)
  : _sandbox_path(path)
{
  unlink(DAEMON_UNIX_PATH);
  this->_d_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
  struct sigaction sig
  {};
  sig.sa_flags = SA_SIGINFO;
  sig.sa_sigaction = &sighandler;
  sigemptyset(&sig.sa_mask);

  if (this->_d_fd == -1) {
    perror("socket");
    goto fail;
  }

  if (bind(this->_d_fd,
           (const struct sockaddr*)&SOCK_DAEMON,
           sizeof SOCK_DAEMON) < 0) {
    perror("bind");
    goto fail;
  }

  if (sigaction(SIGUSR1, &sig, nullptr) != 0) {
    ERROR("sigaction %d failed", SIGUSR1);
  }

  return;

fail:
  if (this->_d_fd >= 0) {
    close(_d_fd);
  }
  throw VULNYEX("%s", "Daemon initialization has failed");
}

FilterDaemon::~FilterDaemon()
{
  if (this->_d_fd) {
    close(this->_d_fd);
  }
  unlink(DAEMON_UNIX_PATH);
}

int
FilterDaemon::_filter_path(std::string& path) const
{

  DBG("Path is %s", path.c_str());

  if (std::regex_match(path, this->_deny) == true) {
    DBG("%s", "Trying to jailbreak");
    return -1;
  }

  if (std::regex_match(path, this->_allow) == true) {
    DBG("%s matches the regex", path.c_str());
    return 0;
  }

  auto max_offset_match = 0;
  DBG("Matching %s with %s", this->_sandbox_path.c_str(), path.c_str());
  for (max_offset_match = 0;
       this->_sandbox_path[max_offset_match] == path[max_offset_match];
       max_offset_match++) {
    DBG("index(%d) %c matches\n", max_offset_match, path[max_offset_match]);
  }

  std::string new_path =
    this->_sandbox_path + std::string("/") + path.substr(max_offset_match);
  DBG("new path is %s", new_path.c_str());
  path = new_path;

  return 1;
}

int
FilterDaemon::_send_response(const struct client_request& rqst)
{
  int ret = -1;
  struct msghdr msg;
  const int& requested_fd = rqst._fd;
  const struct sockaddr_un& to = rqst._from;

  init_msg(msg, sizeof(int), (requested_fd >= 0));

  msg.msg_name = (void*)&to;
  msg.msg_namelen = sizeof to;
  memcpy(msg.msg_iov[0].iov_base, &requested_fd, sizeof requested_fd);

  if (requested_fd >= 0) {
    DBG("Sending back requested fd(%d)", requested_fd);
    struct cmsghdr* const cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    memcpy(CMSG_DATA(cmsg), &requested_fd, sizeof requested_fd);
  }

  if (sendmsg(this->_d_fd, &msg, 0) < 0) {
    ERROR("%s", "Could not send fd back");
  } else {
    DBG("Sent fd(%d) back", requested_fd);
    ret = 0;
  }
  delete_msg(msg, true);
  return ret;
}

int
FilterDaemon::_handle_read(struct client_request& rqst)
{

  int ret = -1;
  struct msghdr msg
  {};
  struct iovec iov
  {};
  iov.iov_base = &rqst._args;
  iov.iov_len = sizeof rqst._args;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_name = &rqst._from;
  msg.msg_namelen = sizeof rqst._from;

  if (recvmsg(this->_d_fd, &msg, 0) < 0) {
    ERROR("%s", "Could not receive");
  } else {
#ifdef DEBUG
    DBG("FROM: %s", ((struct sockaddr_un*)msg.msg_name)->sun_path);
    rqst._args.dump();
#endif
    ret = 0;
  }
  return ret;
}

size_t
FilterDaemon::run()
{
  DBG("%s", "Starting daemon");

  struct pollfd fds[MAX_SOCK];
  size_t counter = 0;

  for (int i = 0; i < MAX_SOCK; i++) {
    fds[i].fd = -1;
  }
  fds[MASTER_SOCKET].fd = this->_d_fd;
  fds[MASTER_SOCKET].events = POLLIN;

  while (OK) {
    int ret = poll(fds, MAX_SOCK, 1000);
    if (ret == 0) { // TIMEOUT
      DBG("TIMEOUT %d", 1000);
      continue;
    } else if (ret == -1) { // Error --wat do?!
      ERROR("%s", "poll has failed..should i die?");
    } else {
      if (fds[MASTER_SOCKET].events & POLLIN) {
        struct client_request request;
        ret = this->_handle_read(request);
        if (ret == -1) { // Error
          ERROR("%s", "Could not read from master fd...");
          continue;
        }
        std::string new_path(request._args._path);
        ret = this->_filter_path(new_path);
        if (ret < 0) {
          DBG("%s", "Attempting a relative file access..denying..");
          request._fd = -EACCES;
        } else {
          strncpy(request._args._path, new_path.c_str(), PATH_MAX);
          ret = this->_try_open(request);
          DBG("Opened file(%s) finished with (%d)", request._args._path, ret);
        }
        ret = this->_send_response(request);
        if (ret < 0) {
          ERROR("%s", "Could not send response");
        } else {
          counter++;
        }
        if (request._fd >= 0) {
          close(request._fd);
        }
      }
    }
  }
  return counter;
}

int
FilterDaemon::_try_open(struct client_request& rqst) const
{
  int backup_errno = errno;
  errno = 0;
  int fd = open(rqst._args._path, rqst._args._flags, rqst._args._mode);
  if (fd < 0) {
    fd = -errno;
  }
  errno = backup_errno;
  rqst._fd = fd;
  return fd;
}

int
main(int argc, char* argv[])
{
  std::string sandbox_path = SANDBOX;
  if (argc > 1) {
    sandbox_path = argv[1];
  }
  FilterDaemon fd(sandbox_path);
  auto count = fd.run();
  printf("Served %zu requests\n", count);
}
