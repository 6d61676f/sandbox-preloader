#ifndef DAEMON_HPP
#define DAEMON_HPP

#include <regex>
#include <string>
#include <sys/un.h>

struct FilterDaemon
{
  std::string _sandbox_path;
  int _d_fd{ -1 };
  // Allow /proc /sys /dev /etc /usr /lib
  static const std::regex _allow;
  // Deny relative paths that would jailbreak
  static const std::regex _deny;
  FilterDaemon(std::string& path);
  FilterDaemon() = delete;
  virtual ~FilterDaemon();
  /**
   * returns no of responses
   */
  size_t run();
  /**
   * -1 invalid
   *  0 valid
   *  1 modified
   */
  int _handle_read(struct client_request& rqst);
  int _filter_path(std::string& path) const;
  int _send_response(const struct client_request& rqst);
  int _try_open(struct client_request& rqst) const;
};

#endif // DAEMON_HPP
