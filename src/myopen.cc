#include "myopen.hpp"
#include "debug.hpp"
#include "sandbox.hpp"
#include <cstdio>
#include <cstdlib>
#include <linux/fcntl.h>
#include <seccomp.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <sys/un.h>
#include <unistd.h>

static scmp_filter_ctx ctx;
static int lib_fd = -1;

__attribute__((constructor)) static void
initialize_bpf()
{
  int i = 0;
  ctx = seccomp_init(SCMP_ACT_KILL);
  int ret = 0;
  if (ctx == nullptr) {
    exit(1);
  }
  /*hackish but maybe it's ok
   * values for syscalls are from asm/unistd_64.h
   */
  for (i = 0; i <= 322; i++) {
    if (i == SYS_open || i == SYS_prctl || i == SYS_seccomp ||
        i == SYS_arch_prctl) {
      DBG("skipping %d", i);
      continue;
    }
    ret = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, i, 0);
    if (ret == -1) {
      ERROR("Failed allowing %d", i);
    }
  }
  ret = seccomp_load(ctx);
  if (ret == -1) {
    ERROR("%s", "Failed loading ruleset");
    goto fail;
  }

  if ((lib_fd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
    ERROR("%s", "socket AF_UNIX");
    goto fail;
  }

  unlink(LIB_UNIX_PATH);
  if (bind(lib_fd, (const struct sockaddr*)&SOCK_LIB, sizeof SOCK_LIB) < 0) {
    ERROR("%s", "bind AF_UNIX");
    goto fail;
  }

  return;

fail:
  if (lib_fd != -1) {
    close(lib_fd);
  }
  if (ctx != nullptr) {
    seccomp_release(ctx);
  }
  exit(1);
}

__attribute__((destructor)) static void
destroy_bpf()
{
  unlink(LIB_UNIX_PATH);
  if (lib_fd != -1) {
    close(lib_fd);
  }
  if (ctx != nullptr) {
    seccomp_release(ctx);
  }
}

int
myopen(const char* pathname, int flags, mode_t mode)
{
  int fd = -1;
  struct open_args args(pathname, flags, mode);
  struct msghdr msg;
  init_msg(msg, sizeof(struct open_args));
  memcpy(msg.msg_iov[0].iov_base, &args, msg.msg_iov[0].iov_len);
  msg.msg_name = &SOCK_DAEMON;
  msg.msg_namelen = sizeof SOCK_DAEMON;
  if (sendmsg(lib_fd, &msg, 0) < 0) {
    ERROR("sendmsg has returned <=%d", 0);
  } else {
    DBG("sent open request for %s", pathname);
    delete_msg(msg);
    init_msg(msg, sizeof(int), true);
    if (recvmsg(lib_fd, &msg, 0) < 0) {
      ERROR("%s", "recvmsg has failed on lib-side");
    } else { // get fd
      DBG("%s", "recvmsg has succeded");
      memcpy(&fd, msg.msg_iov[0].iov_base, msg.msg_iov[0].iov_len);
      if (fd < 0) {
        errno = -fd;
        ERROR("Received errno from daemon: %d", -fd);
        fd = -1;
      } else { // we're ok
        struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
#if 0
        VARDUMP(cmsg, cmsg->cmsg_len, cmsg->cmsg_level, cmsg->cmsg_type);
#endif
        memcpy(&fd, CMSG_DATA(cmsg), sizeof fd);
        DBG("Received fd %d from daemon", fd);
      }
    }
  }
  delete_msg(msg, true);
  return fd;
}

#ifdef __cplusplus
extern "C"
{
#endif
  int openat(int fd, const char* path, int flags, ...)
  {
    DBG("%s", "Hooked openat");
    get_mode;
    if (fd == AT_FDCWD) {
      return open(path, flags, mode);
    } else {
      return syscall(SYS_openat, path, flags, mode);
    }
  }

  int openat64(int fd, const char* path, int flags, ...)
  {
    DBG("%s", "Hooked openat64");
    get_mode;
    if (fd == AT_FDCWD) {
      return open64(path, flags, mode);
    } else {
      return syscall(SYS_openat, path, flags, mode);
    }
  }

  int open(const char* pathname, int flags, ...)
  {
    DBG("%s", "Hooked open");
    get_mode;
    return myopen(pathname, flags, mode);
  }

  int open64(const char* pathname, int flags, ...)
  {
    DBG("%s", "Hooked open64");
    get_mode;
    return myopen(pathname, flags, mode);
  }

#ifdef __cplusplus
}
#endif
