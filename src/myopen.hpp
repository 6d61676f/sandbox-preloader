#ifndef MYOPEN_HPP
#define MYOPEN_HPP

#ifdef __cplusplus
extern "C"
{
#endif
  int open(const char* pathname, int flags, ...);
  int open64(const char* pathname, int flags, ...);
  int openat(int fd, const char* path, int flags, ...);
  int openat64(int fd, const char* path, int flags, ...);
#ifdef __cplusplus
}
#endif

#endif // MYOPEN_HPP