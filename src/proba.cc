#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <string>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

int
main(int argc, char* argv[])
{
  std::string path;
  if (argc == 1) {
    path = "/etc/fstab";
  } else {
    path = argv[1];
  }
  // int fd = syscall(SYS_open, "/etc/fstab", 0, 0);
  char buffer[0x1000];
  int fd = open(path.c_str(), 0);
  ssize_t r = -1;
  if ((r = read(fd, buffer, 0x1000)) != -1) {
    for (auto i = 0; i < r; i++) {
      printf("%c", buffer[i]);
    }
  }
  printf("\n");
  // FILE* stream = fopen("/etc/fstab", "r");
  // if (stream == nullptr) {
  // printf("Problem\n");
  // exit(1);
  //} else {
  // size_t len = 0;
  // char* line = nullptr;
  // while (getline(&line, &len, stream) >= 0) {
  // printf("%s", line);
  //}

  // free(line);
  // fclose(stream);
  //}
  return 0;
}
