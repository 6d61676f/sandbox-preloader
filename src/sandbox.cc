#include "sandbox.hpp"

std::string SANDBOX = SANDBOX_PATH;
struct sockaddr_un SOCK_DAEMON
{
  AF_UNIX, DAEMON_UNIX_PATH
};

struct sockaddr_un SOCK_LIB
{
  AF_UNIX, LIB_UNIX_PATH
};

open_args::open_args()
{
  _path[0] = '\0';
  _flags = 0;
  _mode = 0;
}
open_args::open_args(const char* path, int flags, mode_t mode)
{
  strncpy(_path, path, PATH_MAX);
  this->_flags = flags;
  memcpy(&this->_mode, &mode, sizeof mode);
}

open_args::open_args(const struct open_args& other)
{
  strncpy(_path, other._path, PATH_MAX);
  this->_flags = other._flags;
  memcpy(&this->_mode, &other._mode, sizeof(mode_t));
}

void
open_args::dump() const
{
  printf("PATH(%s), FLAGS(%#X), MODE(%#X)\n", _path, _flags, _mode);
}

client_request::client_request(const struct open_args& args,
                               const struct sockaddr_un& from)
  : _args(args)
  , _fd(-1)
{
  memcpy(&this->_from, &from, sizeof from);
}

client_request::client_request()
{
  _args = {};
  _from = {};
  _fd = -1;
};

void
client_request::dump() const
{
  printf("--------client_request dump------\n");
  this->_args.dump();
  printf("sockaddr_un->sun_family = %d, sockaddr_un->sun_path %s\n",
         this->_from.sun_family,
         this->_from.sun_path);
  printf("--------------------------------\n");
}

void
init_msg(struct msghdr& msg, size_t iov_base_size, bool init_ctrl)
{
  memset(&msg, 0x00, sizeof msg);
  msg.msg_iov = (struct iovec*)calloc(1, sizeof(struct iovec));
  msg.msg_iovlen = 1;
  msg.msg_iov[0].iov_base = calloc(1, iov_base_size);
  msg.msg_iov[0].iov_len = iov_base_size;
  if (init_ctrl) {
    msg.msg_control = calloc(1, CMSG_SPACE(sizeof(int)));
    msg.msg_controllen = CMSG_SPACE(sizeof(int));
  }
}

void
delete_msg(struct msghdr& msg, bool init_ctrl)
{
  if (init_ctrl && msg.msg_control != nullptr) {
    free(msg.msg_control);
    msg.msg_controllen = 0;
  }
  if (msg.msg_iov != nullptr) {
    if (msg.msg_iov[0].iov_base != nullptr) {
      free(msg.msg_iov[0].iov_base);
      msg.msg_iov[0].iov_len = 0;
    }
    free(msg.msg_iov);
    msg.msg_iovlen = 0;
  }
}