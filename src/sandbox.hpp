#ifndef SANDBOX_HPP
#define SANDBOX_HPP
#include <climits>
#include <string>
#include <sys/socket.h>
#include <sys/un.h>

#define get_mode                                                               \
  mode_t mode = 0;                                                             \
  va_list arg;                                                                 \
  va_start(arg, flags);                                                        \
  mode = va_arg(arg, mode_t);                                                  \
  va_end(arg);

extern std::string SANDBOX;
extern struct sockaddr_un SOCK_DAEMON;
extern struct sockaddr_un SOCK_LIB;

struct open_args
{
  char _path[PATH_MAX];
  int _flags;
  mode_t _mode;
  open_args();
  open_args(const char* path, int flags, mode_t mode);
  open_args(const struct open_args& other);
  void dump() const;
};

struct client_request
{
  struct open_args _args;
  struct sockaddr_un _from;
  int _fd;
  client_request(const struct open_args& args, const struct sockaddr_un& from);
  client_request();
  void dump() const;
};

void
init_msg(struct msghdr& msg, size_t size_of_iovbase, bool init_ctrl = false);

void
delete_msg(struct msghdr& msg, bool init_ctrl = true);

#endif // SANDBOX_HPP
